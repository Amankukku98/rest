# REST architecture
# Introduction

REST stands for REpresentational State Transfer. REST was first introduced by Roy Fielding in 2000. REST is an architectural style for developing web services. It is popular due to its simplicity and the fact that it's built upon existing systems. REST is a web standards-based architecture and uses HTTP Protocol.

* REST is not HTTP.
* REST is not a protocol.
* REST is a guideline to build a performant and scalable system on the web.
* REST defines how developers interact with web services.
* The popularity of REST is due to its widespread use in both server and client-side implementations.

In REST architecture, a REST Server simply provides access to resources, and the REST client accesses and modifies the resources. Here each resource is identified by URLs or global IDs. REST uses various representations to represent a resource like text, JSON, and XML. JSON is the most popular one. REST is just a fancy way 
of saying that a server responds to create, read, update, and delete requests in a standard way. The idea behind REST is to treat all server URs as access points for the various resources on the server.

## REST defines four interface constraints:
    
                * Identification of resources 
                *  Manipulation of resources
                *  Self-descriptive messages 
                *  Hypermedia as the engine of application state


Generally, it describes a machine-to-machine interface, and more specifically in web development, it allows for an addition or replacement of server-side rendering to assist client-side rendering in web applications as the client-server model where the web browser acts as the client.

# HTTP methods

               * GET
               * POST
               * DELETE
               * PUT


The default operation of HTTP is GET, which is used to retrieve data from the server. The REST philosophy asserts that to delete something on the server, you would simply use the URL for the resource and specify the DELETE method of HTTP. For saving data to the server, a URL and the PUT method would be used. For operations that are more involved than simply saving, reading, or deleting information, the POST method of HTTP can be used.

REST is a software architectural style that defines the set of rules to be used for creating web services. Web services that follow the REST architectural
style are known as RESTful web services. It allows requesting systems to access and manipulate web resources by using a uniform and predefined set of rules.

A Restful System consists of a:
               
                * client who requests the resources.
                * server who has the resources.

# Architectural Constraints of REST API:
Six architectural constraints make any web service.
  
                 1. Uniform interface
                 2. Client-Server
                 3. Stateless
                 4. Cacheable
                 5. Layered System
                 6. Code of demand(optional)


#### 1. Uniform interface:
While the client and server interact with each other, all the interactions should be in a unified way then it is easy to simplify and decouple them. It is important that the API can facilitate communication. To that end, REST APIs impose a uniform interface that can easily accommodate all connected software.

All resources should be accessible through a common approach such as HTTP GET and similarly modified using a consistent approach.

Also, the resource representations across the system should follow specific guidelines such as naming conventions, link formats, or data format (XML or/and JSON).


#### 2. Client-Server:
The application should follow the client-server architecture. Both the client and Server should be independent of each other. When done properly, the client and server can update and evolve in different directions without having an impact on the quality of their data exchange. This is especially important in various cases where there are plenty of different clients a server has to cater to. Think about whether APIs — have to send data to tons of different clients (all types of mobile devices are good examples) from a single database.



#### 3. Stateless:
Stateless means we do have not to store any data on a server. The server should be stateless. It means that the necessary state to handle the request is contained within the request itself and the server would not store anything related to the session. In REST, the client must include all information for the server to fulfill the request whether as a part of query params, headers or URI



#### 4.Cacheable:
The client has the option to locally store certain pieces of data for a predetermined period. When they request that data, instead of the server sending it again, they use the stored version.



#### 5. Layered System:
An application architecture needs to be composed of multiple layers. Each layer doesn’t know anything about any layer. A client shouldn’t be able to easily tell what system is responding to their request especially if it’s behind an API Gateway.


#### 6. Code on Demand:
This constraint is optional. For the most part, I don’t see this implemented very often because it can be a security issue but it’s okay to return code that code renders a user interface widget. This used to be popular in the early 2000s.


# Summary

REST is a software architectural style created by Roy Fielding in 2000 to guide the design of architecture for the web. which follow the REST architectural
style is known as RESTful web services.

##### HTTP methods
 
* GET
* POST
* DELETE
* PUT


A Restful System consists of a:
               
* client who requests the resources.
* server who has the resources.


##### Architectural Constraints of REST API:
  
                 1. Uniform interface
                 2. Client-Server
                 3. Stateless
                 4. Cacheable
                 5. Layered System
                 6. Code of demand(optional)



# References

* https://www.ibm.com/docs/en/control-desk/7.6.1.2?topic=apis-rest-api

* https://restfulapi.net/rest-architectural-constraints/

* https://youtu.be/lsMQRaeKNDk
